package ptr

func Pointer[T any](value T) *T {
	return &value
}

func P[T any](value T) *T {
	return Pointer(value)
}

func Value[T any](value *T) T {
	if value == nil {
		return *new(T)
	}
	return *value
}

func V[T any](value *T) T {
	return Value(value)
}
